package services

import play.api.libs.json.{Json, OWrites}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import service.products


case class ProductId ( id: String )
object ProductId {
  implicit val productIdWrites: OWrites[ProductId] = Json.writes[ProductId]
}
case class SellerId ( id: String )
object SellerId {
  implicit val sellerIdWrites: OWrites[SellerId] = Json.writes[SellerId]
}
case class ProductName( id: String )
object ProductName {
  implicit val productNameWrites: OWrites[ProductName] = Json.writes[ProductName]
}
case class PricePerUnit( price: Long )
object PricePerUnit {
  implicit val pricePerUnitWrites: OWrites[PricePerUnit] = Json.writes[PricePerUnit]
}

class service {

  def getAllProducts ={
    Future{products}
  }

  def printProducts = {
    for {
      product <- getAllProducts
    } yield {
      product
    }
  }

}

object service {
  /*
  case class Product( sellerId: String,
                      productId: Long,
                      productName: String,
                      pricePerUnit: Long )
*/
  case class Product( sellerId: SellerId,
                      productId: ProductId,
                      productName: ProductName,
                      pricePerUnit: PricePerUnit )

  object Product {
      implicit val userWrites: OWrites[Product] = Json.writes[Product]
  }

/*
  val product1 = Product( "sell1", 1 , "abc" , 200)
  val product2 = Product( "sell1", 3 , "bcd" , 100)
  val product3 = Product( "sell3", 2 , "cde" , 300)
  val product4 = Product( "sell4", 4 , "def" , 400)
  val product5 = Product( "sell5", 5 , "efg" , 200)
  val product6 = Product( "sell1", 6 , "fgh" , 150)
  val product7 = Product( "sell1", 7 , "ghi" , 250)
*/

  val product1 = Product( SellerId("sell1"), ProductId("prod1"), ProductName("abc") , PricePerUnit(200))
  val product2 = Product( SellerId("sell1"), ProductId("prod2"), ProductName("bcd") , PricePerUnit(100))
  val product3 = Product( SellerId("sell3"), ProductId("prod3"), ProductName("cde") , PricePerUnit(300))
  val product4 = Product( SellerId("sell4"), ProductId("prod4"), ProductName("def") , PricePerUnit(400))
  val product5 = Product( SellerId("sell5"), ProductId("prod5"), ProductName("efg") , PricePerUnit(200))
  val product6 = Product( SellerId("sell1"), ProductId("prod6"), ProductName("fgh") , PricePerUnit(150))
  val product7 = Product( SellerId("sell1"), ProductId("prod7"), ProductName("ghi") , PricePerUnit(250))
  val products = List(product1,product2, product3, product4, product5, product6, product7)
//  val products = List( "asdas", "asdasd")
}

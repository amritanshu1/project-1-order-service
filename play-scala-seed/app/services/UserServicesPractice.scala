package services

import com.google.inject.ImplementedBy
import play.api.libs.json._
import services.UserServicesPractice.UserType.{AnonymousUser, NonPremiumUser, PremiumUser}
import services.UserServicesPractice.{User}

import scala.collection.mutable
import scala.concurrent.Future

@ImplementedBy(classOf[UserServicesPracticeImplementation])
trait UserServicesPractice /*@Inject()(userServiceImpl:UserServiceImplementations)*/  {
  def getDocs(id: Long, userType: String) : Future[Option[String]]
  def getAllUsers: Future[mutable.Set[User]]
  def createUser(user: User): Future[mutable.Set[User]]
  def deleteUser(user: User): Future[mutable.Set[User]]
  def createMultipleUser(userList: mutable.Set[User]): Future[mutable.Set[User]]
  def deleteMultipleUser(userSet: mutable.Set[User]): Future[mutable.Set[User]]

}

object UserServicesPractice {

  sealed trait UserType {
    def name : String
  }

  object UserType {

    case object PremiumUser extends UserType {
      override def name: String = "Premium User"
    }
    case object NonPremiumUser extends UserType {
      override def name: String = "Non Premium User"
    }
    case object AnonymousUser extends UserType {
      override def name: String = "Anonymous User"
    }

    val userTypeSet: mutable.Set[UserType] = mutable.Set(PremiumUser, NonPremiumUser, AnonymousUser)

    implicit val reads: Reads[UserType] = JsPath.read[String].map(x => toUserType(x))
    implicit val writes: Writes[UserType] = new Writes[UserType] {
      override def writes(o: UserType): JsValue = JsString(o.name)
    }

//    implicit val writes: Writes[UserType] = (o:UserType) => JsString(o.name)

    def toUserType(str: String): UserType = userTypeSet.collectFirst{
      case userType if userType.name.equals(str) => userType
    }.getOrElse(
      throw InvalidUserTypeString(str)
    )

    case class InvalidUserTypeString(userTypeString: String) extends Exception (
      s"[UserService] Invalid userType String $userTypeString"
    )


    /*
    implicit val reads: Reads[UserType] = Reads {
      case JsString("Premium User") =>  JsSuccess(PremiumUser)
      case JsString("Non Premium User") => JsSuccess(NonPremiumUser)
      case JsString("Anonymous User") => JsSuccess(AnonymousUser)
      case _ => JsError("Cannot parse the user type")
    }

    implicit val writes: Writes[UserType] = Writes {
      user => JsString(user.name)
    }*/

//    implicit def pathBinder[T](implicit stringBinder : PathBindable[String]) = new PathBindable[UserType] {
//      override def bind(key: String, value: String): Either[String, UserType] = stringBinder.bind(key, value) match {
//        case Right() => Right(UserType(PremiumUser))
//        case _ => Left("Unable to bind an Id")
//      }
//
//      override def unbind(key: String, value: UserType): String = value.name
//    }



  }



  case class User(name: String, id: Long, userType: UserType)

  object User {
/*    implicit val userWrites = Json.writes[User]
    implicit val userReads  = Json.reads[User]*/
    implicit val userFormats: OFormat[User] = Json.format[User]

  }

  val users: mutable.Set[User] = mutable.Set (
    User("abc", 123, PremiumUser),
    User("bcd", 821, NonPremiumUser),
    User("cde", 999, AnonymousUser)
  )

}

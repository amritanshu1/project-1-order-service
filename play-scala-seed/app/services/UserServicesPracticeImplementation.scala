package services


import javax.inject.Singleton
import play.api.Logger
import services.UserServicesPractice.User

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


@Singleton
class UserServicesPracticeImplementation extends UserServicesPractice {
  override def getDocs(id: Long, userType: String):Future[Option[String]] = {
    userType match {
      case "Premium"  => Future.successful(Some("Premium PDF"))
      case "NonPremium" => Future.successful(Some("NON Premium PDF"))
      case "Anonymous" => Future.successful(None)
    }
  }

  override def getAllUsers: Future[mutable.Set[UserServicesPractice.User]] = {
    Future{UserServicesPractice.users}
  }

  override def createUser(user: User): Future[mutable.Set[User]] = {

    for {
      userSet <- getAllUsers
    } yield {
      val ans = userSet.add(user)
      Logger.debug(s"Appended A user $ans")
      userSet
    }

  }

  override def deleteUser(user: User): Future[mutable.Set[User]] = {

    for {
      userSet <- getAllUsers
    } yield {
      val ans = userSet -= user
      Logger.debug(s"Deleted A user $ans")
      userSet
    }
  }

  // : Future[mutable.Set[User]]
  override def createMultipleUser(userSetToAdd: mutable.Set[User]): Future[mutable.Set[User]] = {

    for {
      userSet <- getAllUsers
    } yield {
      userSet ++= userSetToAdd
      userSet
    }
  }

  override def deleteMultipleUser(userSetToDelete:mutable.Set[User]): Future[mutable.Set[User]] = {

    for{
      userSet <- getAllUsers
    } yield {
      userSet --= userSetToDelete
      userSet
    }
  }

}

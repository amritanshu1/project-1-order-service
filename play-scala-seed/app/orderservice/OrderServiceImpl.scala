package orderservice

import javax.inject.Singleton
import orderservice.OrderService.{InvalidOrderIdSearch, Order, OrderId, orders}
import play.api.Logger
import userservice.UserService.UserId

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.collection.mutable.Set
import scala.concurrent.Future

@Singleton
class OrderServiceImpl extends OrderService {

  override def getAllOrders: Future[mutable.Set[Order]] = {
    Future{orders}
  }
  override def getOrderDetailsById(orderId: OrderId): Future[Order] = {

    for {
      orders <- getAllOrders
    } yield {
      orders.find( _.orderId == orderId ) match {
        case Some(order) => order
        case None =>
          Logger.error(" [getUserDetailsByID] User not Found")
          throw InvalidOrderIdSearch(orderId)
      }
    }
  }
  override def addOrderDetails(order: Order): Future[Either[String, Unit]] = {

    for {
      orders <- getAllOrders
    } yield {
      if (orders.add(order)) {
        Left("Order Added Successfully")
      } else {
        Logger.error("[addOrderDetails] Order already exists")
        Right()
      }

    }
  }
  override def getOrderDetailsByUserId(userId: UserId): Future[Option[List[Order]]] = {

    for {
      orders <- getAllOrders
    } yield {
      Some(orders.collect {
        case order if order.userId == userId => order
      }.toList)

    }
  }



}

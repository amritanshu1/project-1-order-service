package orderservice


import com.google.inject.ImplementedBy
import orderservice.OrderService._
import play.api.libs.json._
import userservice.UserService.UserId

import scala.collection.mutable
import scala.concurrent.Future

@ImplementedBy(classOf[OrderServiceImpl])
trait OrderService {

  def getAllOrders: Future[mutable.Set[Order]]
  def getOrderDetailsById(orderId: OrderId): Future[Order]
  def addOrderDetails(order: Order): Future[Either[String, Unit]]
  def getOrderDetailsByUserId( userId: UserId ): Future[Option[List[Order]]]

}

object OrderService {

  case class OrderId( id: String )
  object OrderId {
    implicit val orderIdFormats: OFormat[OrderId] = Json.format[OrderId]
  }

  case class ProductId ( id: String )
  object ProductId {
    implicit val productIdFormats: OFormat[ProductId] = Json.format[ProductId]
  }

  case class SellerId ( id: String )
  object SellerId {
    implicit val sellerIdFormats: OFormat[SellerId] = Json.format[SellerId]
  }

  case class Order ( orderId: OrderId, userId: UserId,
                     prodIdQuantityMap: Map[String, Int], sellerId: SellerId)

  object Order {

    implicit val mapWithProductIdKeyReads: Reads[Map[ProductId, Int]] = new Reads[Map[ProductId, Int]] {
      override def reads(json: JsValue): JsResult[Map[ProductId, Int]] = {
        json.validate[Map[String, Int]] match {
          case JsSuccess(mapWithStringAsKey, _) => JsSuccess(mapWithStringAsKey map {
            case (k,v) => (ProductId(k),v)
          })
          case JsError(errors) => JsError(errors)
        }
      }
    }

    implicit val orderFormats: OFormat[Order] = Json.format[Order]
  }

  case class InvalidOrderIdSearch(orderId: OrderId) extends Exception {
    s"[OrderService] [getOrderDetailsById] Order ID $orderId Not Found"
  }

  case class ErrorInProductReads(x : Map[ProductId,Int]) extends Exception {
    s"Error in Reading Products to Quantity Map"
  }

  val orders: mutable.Set[Order] = mutable.Set(
    Order(OrderId("ORD0001"), UserId("USER6"), Map("PROD49" -> 1,"PROD25" -> 3), SellerId("SELLER3")),
    Order(OrderId("ORD0002"), UserId("USER9"), Map("PROD39" -> 1,"PROD37" -> 3), SellerId("SELLER3")),
    Order(OrderId("ORD0003"), UserId("USER5"), Map("PROD32" -> 1,"PROD43" -> 3), SellerId("SELLER3")),
    Order(OrderId("ORD0004"), UserId("USER8"), Map("PROD40" -> 1,"PROD9"  -> 3), SellerId("SELLER2")),
    Order(OrderId("ORD0005"), UserId("USER3"), Map("PROD31" -> 1,"PROD23" -> 3), SellerId("SELLER2")),
    Order(OrderId("ORD0006"), UserId("USER4"), Map("PROD21" -> 1,"PROD38" -> 3), SellerId("SELLER1")),
    Order(OrderId("ORD0007"), UserId("USER2"), Map("PROD26" -> 1,"PROD22" -> 3), SellerId("SELLER2")),
    Order(OrderId("ORD0008"), UserId("USER2"), Map("PROD48" -> 1,"PROD28" -> 3), SellerId("SELLER1")),
    Order(OrderId("ORD0009"), UserId("USER7"), Map("PROD27" -> 1,"PROD16" -> 3), SellerId("SELLER2")),
    Order(OrderId("ORD0010"), UserId("USER7"), Map("PROD27" -> 1,"PROD25" -> 3), SellerId("SELLER3")),
    Order(OrderId("ORD0011"), UserId("USER10"),Map("PROD44" -> 1,"PROD37" -> 3), SellerId("SELLER2")),
    Order(OrderId("ORD0012"), UserId("USER10"),Map("PROD49" -> 1,"PROD32" -> 3), SellerId("SELLER1")),
    Order(OrderId("ORD0013"), UserId("USER3"), Map("PROD15" -> 1,"PROD45" -> 3), SellerId("SELLER1")),
    Order(OrderId("ORD0014"), UserId("USER10"),Map("PROD5"  -> 1,"PROD18" -> 3), SellerId("SELLER3")),
    Order(OrderId("ORD0015"), UserId("USER10"),Map("PROD30" -> 1,"PROD48" -> 3), SellerId("SELLER2")),
    Order(OrderId("ORD0016"), UserId("USER7"), Map("PROD25" -> 1,"PROD42" -> 3), SellerId("SELLER3")),
    Order(OrderId("ORD0017"), UserId("USER3"), Map("PROD11" -> 1,"PROD40" -> 3), SellerId("SELLER1")),
    Order(OrderId("ORD0018"), UserId("USER9"), Map("PROD19" -> 1,"PROD39" -> 3), SellerId("SELLER2"))
  )
}

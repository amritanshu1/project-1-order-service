package dbioservicepractice


import dbioservicepractice.DbioPractice.Order._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import slick.jdbc.H2Profile.api._

import scala.concurrent._
import scala.concurrent.duration._

object DbioPractice {

  sealed abstract class Rating(val stars: Int)

  object Rating {
    final case object Awesome extends Rating(5)
    final case object Good    extends Rating(4)
    final case object NotBad  extends Rating(3)
    final case object Meh     extends Rating(2)
    final case object Aaargh  extends Rating(1)

    implicit val columnType: BaseColumnType[Rating] =
      MappedColumnType.base[Rating, Int](Rating.toInt, Rating.fromInt)

    private def fromInt(stars: Int): Rating = stars match {
      case 5 => Awesome
      case 4 => Good
      case 3 => NotBad
      case 2 => Meh
      case 1 => Aaargh
      case _ => sys.error("Ratings only apply from 1 to 5")
    }

    private def toInt(rating: Rating): Int = rating match {
      case Awesome => 5
      case Good    => 4
      case NotBad  => 3
      case Meh     => 2
      case Aaargh  => 1
    }
  }

  case class ProductId ( id: Long )
  object ProductId {

    implicit val productIdColumnType: BaseColumnType[ProductId] =
      MappedColumnType.base[ProductId,Long](ProductId.toLong, ProductId.fromLong)

    private def toLong(productId: ProductId): Long = productId.id

    private def fromLong( id : Long ): ProductId = ProductId(id)

  }

  case class Product( productName : String,
                      productPricePerUnit  : Long = 0L,
                      productId     : ProductId )
  case class Order( orderId: String,
                    totalAmount  : Long = 0L,
                    orderedDate     : DateTime,
                    rating : Rating)
    object Order {
     /* def tupled(arg: (String, Long, String)): Order = {
        new Order(arg._1, arg._2, arg._3)
      }

      def unapply(arg: Order): Option[(String, Long, String)] = {
        Some(arg.orderId, arg.totalAmount, arg.orderedDate)
      }
*/
     def dateTimeToString (dateTime: DateTime): String  = {
       val formatter = DateTimeFormat.forPattern("dd/mm/yyyy")
       dateTime.toString(formatter)
     }
      def stringToDateTime (strDateTime: String): DateTime  = {
        val formatter = DateTimeFormat.forPattern("dd/mm/yyyy")
        formatter.parseDateTime(strDateTime)
      }

      implicit val dateTimeColumnType: BaseColumnType[DateTime] =
        MappedColumnType.base[DateTime,String](dateTimeToString, stringToDateTime)
    }

  class ProductTable(tag: Tag) extends Table[Product](tag, "Products") {
    def productName = column[String]("productName")
    def productPricePerUnit  = column[Long]("productPricePerUnit")
    def productId     = column[ProductId]("id", O.PrimaryKey)

    def * = (productName, productPricePerUnit, productId) <> (Product.tupled, Product.unapply)
  }

  class OrderTable(tag: Tag) extends Table[Order] (tag, "Orders") {

    def orderId = column[String]("orderId", O.PrimaryKey)
    def totalAmount = column[Long]("totalAmount")
    def orderedDate = column[DateTime]("orderedDate")
    def rating = column[Rating]("rating")
    def * = (orderId, totalAmount, orderedDate, rating) <> ((Order.apply _).tupled, Order.unapply)
  }



  // objects which will be used as a root for all the queries

  lazy val orderTable = TableQuery[OrderTable]
  lazy val productTable = TableQuery[ProductTable]


  // actions are like commands , creation command, command is not called here just initialised

  val createProductTableAction = productTable.schema.create
  val createOrderTableAction = orderTable.schema.create



  // populate command

  val insertProductAction = productTable ++= Seq(
    Product("prod1", 10909, ProductId(778)),
    Product("prod2", 98100, ProductId(832)),
    Product("prod3", 11232, ProductId(420))

  )

  val insertOrderAction = orderTable ++= Seq(
    Order("ord1", 8890,DateTime.parse("22/07/2019", DateTimeFormat.forPattern("dd/MM/yyyy")), Rating.Good),
    Order("ord2", 120,DateTime.parse("22/07/2019", DateTimeFormat.forPattern("dd/MM/yyyy")), Rating.Good),
    Order("ord3", 90 ,DateTime.parse("22/07/2019", DateTimeFormat.forPattern("dd/MM/yyyy")), Rating.Good),
    Order("ord4", 910,DateTime.parse("22/07/2019", DateTimeFormat.forPattern("dd/MM/yyyy")), Rating.Good),
    Order("ord5", 430,DateTime.parse("22/07/2019", DateTimeFormat.forPattern("dd/MM/yyyy")), Rating.Awesome)
  )


  // select * from AnyTable

  val selectProductAction = productTable.result
  val selectOrderAction = orderTable.result

  val db = Database.forConfig("h2Db")


  def exec[T](action: DBIO[T]): T =
    Await.result(db.run(action), 2.seconds)

  def main(args: Array[String]): Unit = {
    exec(createOrderTableAction)
    exec(insertOrderAction)
    exec(selectOrderAction).foreach(println)
    exec(createProductTableAction)
    exec(insertProductAction)
//    println(exec(productTable.filter( ).result))
    exec(selectProductAction).foreach(println)

  }


}
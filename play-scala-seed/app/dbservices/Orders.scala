package dbservices

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import slick.jdbc.PostgresProfile.api._
import Orders._

case class OrderId( id: String )
object OrderId {
  implicit val orderIdColumnType: BaseColumnType[OrderId] =
    MappedColumnType.base[OrderId, String](orderIdToString, stringToOrderId)
  def orderIdToString(orderId: OrderId): String = orderId.id
  def stringToOrderId(str: String) = OrderId(str)
}


case class Orders ( orderId: OrderId,
                     amount: Long,
                     createdAt: DateTime )

object Orders {

  def dateTimeToString (dateTime: DateTime): String  = {
    val formatter = DateTimeFormat.forPattern("dd/mm/yyyy")
    dateTime.toString(formatter)
  }
  def stringToDateTime (strDateTime: String): DateTime  = {
    val formatter = DateTimeFormat.forPattern("dd/mm/yyyy")
    formatter.parseDateTime(strDateTime)
  }

  implicit val dateTimeColumnType: BaseColumnType[DateTime] =
    MappedColumnType.base[DateTime,String](dateTimeToString, stringToDateTime)

}


class OrderTable(tag : Tag) extends Table[Orders](tag, "Orders") {

  def orderId = column[OrderId]("orderId", O.PrimaryKey)
  def amount = column[Long]("amount")
  def createdAt = column[DateTime]("amount")

  override def * = (orderId, amount, createdAt).<>((Orders.apply _).tupled, Orders.unapply)
}

package dbservices

import org.joda.time.DateTime
import slick.jdbc.PostgresProfile.api._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.libs.json.{JsString, JsValue, Reads, Writes}

sealed trait OrderStatus{
  def status : String
}

object OrderStatus {

  case object ORDER_CONFIRMED extends OrderStatus {
    override val status: String = "ORDER_CONFIRMED"
  }
  case object READY_TO_DELIVER extends OrderStatus {
    override val status: String = "READY_TO_DELIVER"
  }
  case object IN_TRANSFER extends OrderStatus {
    override val status: String = "IN_TRANSFER"
  }
  case object OUT_FOR_DELIVERY extends OrderStatus {
    override val status: String = "OUT_FOR_DELIVERY"
  }
  case object DELIVERED extends OrderStatus {
    override val status: String = "DELIVERED"
  }

  implicit val orderStatusColType: BaseColumnType[OrderStatus] =
    MappedColumnType.base[OrderStatus, String](statusToString, stringToStatus)


  val orderStatusSet: Set[OrderStatus] = Set[OrderStatus](ORDER_CONFIRMED, DELIVERED, OUT_FOR_DELIVERY,
    IN_TRANSFER, READY_TO_DELIVER )

  def statusToString(orderStatus: OrderStatus): String = orderStatus.status
  def stringToStatus(string: String): OrderStatus = orderStatusSet.collectFirst{
    case orderStatus if orderStatus.status == string => orderStatus
  }.getOrElse{
    throw new Exception("Incorrect Order Status Type String Provided")
  }

}

case class OrderHistory( historyId: Long=0L,
                         orderId: OrderId,
                         status: OrderStatus,
                         createdAt: DateTime,
                         updatedAt: DateTime,
                         superSeededBy: Option[Long])


class OrderHistoryTable(tag: Tag) extends Table[OrderHistory](tag, "OrderHistory") {


  val dateFormat = "yyyy-MM-dd"
  val jodaDateReads: Reads[DateTime] = Reads[DateTime](js =>
    js.validate[String].map[DateTime](dtString =>
      DateTime.parse(dtString, DateTimeFormat.forPattern(dateFormat))
    )
  )

  val jodaDateWrites: Writes[DateTime] = new Writes[DateTime] {
    def writes(d: DateTime): JsValue = JsString(d.toString())
  }

  implicit val dateTimeColumnType: BaseColumnType[DateTime] =
    MappedColumnType.base[DateTime,String](dt => dt.toString,
      dtStr => DateTime.parse(dtStr, DateTimeFormat.forPattern(dateFormat)))


  def historyId = column[Long]("historyId", O.PrimaryKey)
  def orderId = column[OrderId]("orderId")
  def status = column[OrderStatus]("orderStatus")
  def createdAt = column[DateTime]("createdAt")
  def updatedAt = column[DateTime]("updatedAT")
  def superSeededBy = column[Option[Long]]("superSeededBy")

  override def * = (historyId, orderId, status, createdAt, updatedAt, superSeededBy)
    .<>(OrderHistory.tupled, OrderHistory.unapply)


}
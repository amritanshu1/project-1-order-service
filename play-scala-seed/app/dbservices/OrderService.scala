package dbservices
import dbservices.OrderService._
import dbservices.OrderHistory._
import org.joda.time.DateTime
import slick.jdbc.PostgresProfile.api._
import slick.lifted.TableQuery

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class OrderService {

  def dbRun[T](action: DBIO[T]): Future[T] =
    db.run(action)

  def addOrderDBIO (orderId: OrderId, amount: Long): DBIO[Unit] = {
    val dateTime = DateTime.now()
    (for {
      _ <- orderTableQuery += Orders(orderId, amount, dateTime)
      _ <- orderHistoryTableQuery += OrderHistory(-1L, orderId, OrderStatus.ORDER_CONFIRMED, dateTime, dateTime, None)
    } yield {}).transactionally
  }

  def addOrder(orderId: OrderId, amount: Long): Future[Unit] = {
      dbRun(addOrderDBIO(orderId, amount))
  }


//  def updateOrderStatusDBIO(status: OrderStatus, orderId: OrderId):DBIO[Unit] = {
//
//    val dateTime = DateTime.now()
//    ( for {
//        oldOrder <- orderHistoryTableQuery.filter( row =>
//        row.orderId === orderId && row.superSeededBy.isEmpty ).result.head
//
//        newHistoryId <- orderHistoryTableQuery returning orderHistoryTableQuery.map(_.historyId)+=
//          OrderHistory(-1L,orderId, status, dateTime, dateTime, None)
//
//        _ <- orderHistoryTableQuery.filter( _.historyId === oldOrder.historyId).map( row =>
//          (row.updatedAt, row.superSeededBy)).update(dateTime, newHistoryId)
//    } yield ()
//    ).transactionally
//
//  }
//
//  def updateOrderStatus(status: OrderStatus, orderId: OrderId): Future[Unit] = {
//    dbRun(updateOrderStatusDBIO(status, orderId))
//  }



  def getOrderStatusHistoryDBIO(orderId: OrderId): DBIO[Seq[OrderHistoryDTO]] = {
    orderHistoryTableQuery.filter(x => x.orderId === orderId)
      .result.map(recLst => recLst.map(rec => OrderHistoryDTO(rec.orderId, rec.status, rec.updatedAt)))
  }

  def getOrderStatusHistory(orderId: OrderId): Future[Seq[OrderHistoryDTO]] = {
    dbRun(getOrderStatusHistoryDBIO(orderId))
  }


  def getCurrentOrderStatusDBIO(orderId: OrderId): DBIO[OrderStatus] = {
    orderHistoryTableQuery.filter(row => row.orderId === orderId && row.superSeededBy.isEmpty).map(_.status)
      .result.head
  }

}


object OrderService {

  case class OrderHistoryDTO(orderId: OrderId, status: OrderStatus, updatedAt: DateTime)


  lazy val orderTableQuery = TableQuery[OrderTable]
  lazy val orderHistoryTableQuery = TableQuery[OrderHistoryTable]

  val createTableOrderAction = orderTableQuery.schema.create
  val createTableOrderHistoryAction = orderHistoryTableQuery.schema.create

  val selectAllOrders = orderTableQuery.result
  val selectAllOrderHistory = orderHistoryTableQuery.result

  val db = Database.forConfig("postgresDb")


}
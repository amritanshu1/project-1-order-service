package userservice

import javax.inject.Singleton
import play.api.Logger
import userservice.UserService.{InvalidUserIdSearch, User, UserId, users}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.collection.mutable
import scala.concurrent.Future

@Singleton
class UserServiceImpl extends UserService {

  override def getAllUsers: Future[mutable.Set[UserService.User]] = {
    Future{users}
  }
  override def getUserDetailsByID(userId: UserId): Future[User] = {

    for {
      users <- getAllUsers
    } yield {
      users.find( _.userId == userId ) match {
        case Some(user) => user
        case None =>
          Logger.error(" [getUserDetailsByID] User not Found")
          throw InvalidUserIdSearch(userId)
      }
    }

  }
  override def addUserDetails(user : User): Future[Either[String, Unit]] = {

    for {
      users <- getAllUsers
    } yield {
      if ( users.add(user) ) {
        Left("User Added Successfully")
      } else {
        Logger.error("[addUserDetails] User already exists")
        Right()
      }
    }

  }

}

package userservice

import com.google.inject.ImplementedBy
import play.api.libs.json._
import userservice.UserService.Client.{Mobile, Web}
import userservice.UserService.{User, _}

import scala.collection.mutable
import scala.collection.mutable.Set
import scala.concurrent.Future



@ImplementedBy(classOf[UserServiceImpl])
trait UserService {

  def getAllUsers: Future[mutable.Set[User]]
  def getUserDetailsByID(userId: UserId): Future[User]
  def addUserDetails(user : User): Future[Either[String, Unit]]

}

object UserService {

  sealed trait Client {
    def name: String
  }
  object Client {
    case object Mobile extends Client {
      override val name: String = "Mobile Client"
    }
    case object Web extends Client {
      override val name: String = "Web Client"
    }

    val clientSet: mutable.Set[Client] = Set(Mobile, Web)

    implicit val clientReads: Reads[Client] = JsPath.read[String].map (toClientType)
    implicit val clientWrites: Writes[Client] = new Writes[Client] {
      override def writes(o: Client): JsValue = JsString(o.name)
    }

    def toClientType(str: String):Client = clientSet.collectFirst{
      case client if client.name.equals(str) => client
    }.getOrElse{
      throw InvalidClientType(str)
    }

    case class InvalidClientType(clientType: String) extends Exception (
      s"[UserService] Invalid userType String $clientType"
    )

  }



  case class AppId (  id: String )
  object AppId {
    implicit val appIdFormat: OFormat[AppId] = Json.format[AppId]
  }

  case class UserId ( id: String )
  object UserId {
    implicit val userIDFormat: OFormat[UserId] = Json.format[UserId]
  }

  case class User(userId: UserId, emailId: String, phoneNumber: Option[String],
                  userName: String, clientType: Client, appId: Option[AppId])

  object User {
    implicit val userFormats: OFormat[User] = Json.format[User]
  }

  case class InvalidUserIdSearch(userId: UserId) extends Exception {
    s" [UserService][getUserDetailsById] User ID $userId Not Found"
  }

  val users = Set(
      User(UserId("USER001"), "a@zilingo.com", None, "x", Web, Some(AppId("APP732433690091139"))),
      User(UserId("USER002"), "b@zilingo.com", Some("3160808151"), "y", Web, Some(AppId("APP568324905393961"))),
      User(UserId("USER003"), "c@zilingo.com", None, "z", Mobile, None),
      User(UserId("USER004"), "d@zilingo.com", Some("1967970240"), "Alpha", Web, Some(AppId("APP129548038866877"))),
      User(UserId("USER005"), "e@zilingo.com", None, "beta", Web, None),
      User(UserId("USER006"), "f@zilingo.com", Some("3401384237"), "zeta", Mobile, Some(AppId("APP144467606423751"))),
      User(UserId("USER007"), "g@zilingo.com", None, "peta", Web, Some(AppId("APP662727877364786"))),
      User(UserId("USER008"), "h@zilingo.com", Some("5808598284"), "kappa", Web, None),
      User(UserId("USER009"), "i@zilingo.com", Some("2133753455"), "gama", Web, None),
      User(UserId("USER010"), "j@zilingo.com", Some("9447720508"), "lambda", Mobile, Some(AppId("APP721162738985081")))
  )

}

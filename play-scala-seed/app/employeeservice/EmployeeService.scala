package employeeservice

import play.api.libs.json.{JsPath, JsResult, JsString, JsValue, Json, OFormat, Reads, Writes}

trait EmployeeService {

}

object EmployeeService {


  case class EmployeeId(id: String)

  object EmployeeId {

    implicit val employeeIdRead: Reads[EmployeeId] = new Reads[EmployeeId] {
      override def reads(json: JsValue): JsResult[EmployeeId] = json.validate[String].map(EmployeeId(_))
    }

    implicit val employeeIdWrites: Writes[EmployeeId] = new Writes[EmployeeId] {
      override def writes(o: EmployeeId): JsValue = JsString(o.id)
    }


    def apply(teamNameContainingOnlyAlphabets: String,
              employeeFirstName: String,
              employeeNumberInCompany: Int) = new
        EmployeeId(teamNameContainingOnlyAlphabets + "_" + employeeFirstName + "_" + employeeNumberInCompany.toString)
  }

  sealed trait EmployeeStatus {
    def status: String
  }

  object EmployeeStatus {

    case object PROBATION extends EmployeeStatus {
      override def status = "PROBATION"
    }

    case object FULL_TIME extends EmployeeStatus {
      override def status = "FULL_TIME"
    }

    case object PART_TIME extends EmployeeStatus {
      override def status = "PART_TIME"
    }

    case object CONTRACT extends EmployeeStatus {
      override def status = "CONTRACT"
    }

    case object TERMINATED extends EmployeeStatus {
      override def status = "TERMINATED"
    }

    val statusSet: Set[EmployeeStatus] = Set(TERMINATED,CONTRACT, PART_TIME, FULL_TIME, PROBATION)

    implicit val employeeStatusReads: Reads[EmployeeStatus] = JsPath.read[String].map (toStatusType)
    implicit val employeeStatusWrites: Writes[EmployeeStatus] = new Writes[EmployeeStatus] {
      override def writes(o: EmployeeStatus): JsValue = JsString(o.status)
    }

    def toStatusType(str: String):EmployeeStatus = statusSet.collectFirst{
      case statusType if statusType.status.equals(str) => statusType
    }.getOrElse{
      throw new Exception("  ERROR in CONVERTING STATUS FROM STRING  ")
    }


  }

  case class Employee(id: EmployeeId, status: EmployeeStatus)

  object Employee {
    implicit val employeeFormat: OFormat[Employee] = Json.format[Employee]
  }
}

package weatherserviceFinal

import com.google.inject.ImplementedBy
import org.joda.time.DateTime
import play.api.libs.json.{Json, OFormat}
import weatherserviceFinal.WeatherService.ForecastData
import scala.collection.mutable
import scala.concurrent.Future


@ImplementedBy(classOf[WeatherServiceImpl])
trait WeatherService {

  def getAllForecastDataSet: Future[mutable.Set[ForecastData]]
  def fetchWeatherData(city: String, days: Int): Future[ForecastData]
  def storeWeatherData(forecastData: ForecastData): Future[Unit]
  def getWeatherDataHistory(from: DateTime, to: DateTime): Future[mutable.Set[ForecastData]]
  def getWeatherDataForCity(city: String): Future[ForecastData]

}


object WeatherService {

  case class Location(name: String, country: String)
  object Location {
    implicit val locationFormat: OFormat[Location] = Json.format[Location]
  }


  case class Day(maxtemp_c: Double, mintemp_c: Double)
  object Day {
    implicit val dayFormat: OFormat[Day] = Json.format[Day]
  }


  case class Forecastday(date: DateTime, day: Day )
  object Forecastday {
    implicit val forecastDayFormat: OFormat[Forecastday] = Json.format[Forecastday]
  }


  case class Forecast( forecastday: List[Forecastday] )
  object Forecast {
    implicit val forecastFormat: OFormat[Forecast] = Json.format[Forecast]
  }


  case class ForecastData( location: Location, forecast: Forecast)
  object ForecastData {
    implicit val forecastDataFormat: OFormat[ForecastData] = Json.format[ForecastData]
  }

  val forecastDataSet: mutable.Set[ForecastData] = mutable.Set()

}

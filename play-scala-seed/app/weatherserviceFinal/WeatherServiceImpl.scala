package weatherserviceFinal

import javax.inject.{Inject, Singleton}
import play.api.{Configuration, Logger}
import play.api.libs.json.{JsError, JsSuccess}
import play.api.libs.ws.WSClient
import weatherserviceFinal.WeatherService.{Forecast, ForecastData}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class WeatherServiceImpl @Inject()(wsClient: WSClient, config: Configuration) extends WeatherService {

  override def getAllForecastDataSet: Future[mutable.Set[ForecastData]] = {
    Future{WeatherService.forecastDataSet}
  }

  override def fetchWeatherData(city: String, days: Int): Future[ForecastData] = {

    val weatherURL = config.getString("weatherURL").get
    val customUrl = s"$weatherURL&q=$city&days=$days"

    val wsResponse = wsClient.url(customUrl)
      .get().map(_.json)

    wsResponse.map( response =>
      response.validate[ForecastData] match {
        case JsSuccess( forecastData, _ ) => forecastData
        case JsError(errors) =>
          Logger.debug( s"Error in validating Data $errors" )
          throw new Exception(s"Exception while validating Data $errors")
    } )

  }

  override def storeWeatherData(forecastData: ForecastData): Future[Unit] = {

    for {
      forecastDataSet <- getAllForecastDataSet
    } {
      forecastDataSet.add(forecastData)
    }
    Future(())
  }

  override def getWeatherDataHistory(from: DateTime, to: DateTime): Future[mutable.Set[ForecastData]] = {

    for {
      forecastDataSet <- getAllForecastDataSet
    } yield {

      for {
        data <- forecastDataSet
      } yield {
        val forecastDayList = data.forecast.forecastday.collect{
          case forecastDay if forecastDay.date.isAfter(from) && forecastDay.date.isBefore(to) => forecastDay
        }
        ForecastData(data.location,Forecast(forecastDayList))
      }

    }
  }

  def getWeatherDataForCity(city: String): Future[ForecastData] = {

    for {
      forecastDataSet <- getAllForecastDataSet
    } yield {
      forecastDataSet.find( _.location.name == city ) match {
        case Some(cityWeather) => cityWeather
        case _ =>
          Logger.error(s"Could not find weather for the city $city")
          throw new Exception(s"Could not find weather for $city")
      }
    }

  }
}

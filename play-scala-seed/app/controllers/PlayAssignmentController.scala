package controllers

import com.google.inject.Inject
import orderservice.OrderService
import orderservice.OrderService.{Order, OrderId}
import play.api.Logger
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import play.api.mvc.{Action, AnyContent, Controller, Result}
import userservice.UserService
import userservice.UserService._
import userservice.UserService.{User, UserId}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


class PlayAssignmentController @Inject()(userService: UserService, orderService: OrderService) extends Controller{

  def getAllUsers: Action[AnyContent] = Action.async {
    userService.getAllUsers.map(x => Ok(Json.toJson(x)) )
  }

  def getAllOrders: Action[AnyContent] = Action.async {
    orderService.getAllOrders.map( x=> Ok(Json.toJson(x))  )
  }

  def getUserDetailsByID: Action[JsValue] = Action.async(parse.json) { request =>

    request.body.validate[UserId] match {
      case JsSuccess(value, _) => userService.getUserDetailsByID(value).map( x=> Ok(Json.toJson(x)))
      case JsError(errors) =>  Future.successful(BadRequest(s"$errors"))
    }
  }

  def getOrderDetailsByID: Action[JsValue] = Action.async(parse.json) { request =>

    request.body.validate[OrderId] match {
      case JsSuccess(value, _) => orderService.getOrderDetailsById(value).map( x=> Ok(Json.toJson(x)))
      case JsError(errors) =>  Future.successful(BadRequest(s"$errors"))
    }
  }

  def getOrderDetailsByUserId: Action[JsValue] = Action.async(parse.json) { request =>
    request.body.validate[UserId] match  {
      case JsSuccess(value, _) => orderService.getOrderDetailsByUserId(value).map( x => Ok(Json.toJson(x)) )
      case JsError(errors) => Future.successful(BadRequest(s"$errors"))
    }

  }

  def addUserDetails: Action[JsValue] = Action.async(parse.json) { request =>
    request.body.validate[User] match {
      case JsSuccess(value, _) => userService.addUserDetails(value).map {
        case Left(string) => Ok(Json.toJson(string))
        case Right(_)  =>
          Ok(Json.toJson("[addUserDetails] User already exists"))
//          BadRequest
      }
      case JsError(errors) =>
        Future.successful(BadRequest(s"$errors"))

    }
  }

  def addOrderDetails: Action[JsValue] = Action.async(parse.json) { request =>
    request.body.validate[Order] match {
      case JsSuccess(value, _) => orderService.addOrderDetails(value).map {
        case Left(string) => Ok(Json.toJson(string))
        case Right(_)  =>
          Ok(Json.toJson("[addOrderDetails] Order already exists"))
        //          BadRequest
      }
      case JsError(errors) =>
        Future.successful(BadRequest(s"$errors"))

    }
  }
}

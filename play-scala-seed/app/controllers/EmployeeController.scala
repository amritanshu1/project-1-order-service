package controllers

import com.google.inject.Inject
import employeeservice.EmployeeService
import employeeservice.EmployeeService.Employee
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import play.api.mvc.{Action, Controller}

import scala.concurrent.Future

class EmployeeController extends Controller{

  def index: Action[JsValue] = Action.async(parse.json) { implicit request =>
    request.body.validate[Employee] match {
      case JsSuccess(value, _) => Future.successful(Ok(Json.toJson(value)))
      case JsError(errors) => Future.successful(BadRequest(s"Oh my god look:-  $errors"))
    }

  }

}

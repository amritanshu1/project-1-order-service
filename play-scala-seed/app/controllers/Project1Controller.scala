package controllers

import Project1.models.Orders
import Project1.services.OrderService
import com.google.inject.{Inject, Singleton}
import play.api.Logger
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import play.api.mvc.{Action, Controller}
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class Project1Controller @Inject()(orderService: OrderService) extends Controller{


  def insertOrders: Action[JsValue] = Action.async(parse.json) { implicit request =>

    request.body.validate[Seq[Orders]] match {
      case JsSuccess(value, _) => orderService.insertOrder(value).map{_
        => Ok(Json.obj("INSERT_ORDERS"->"SUCCESS"))} .recover{
          case exception =>
            Logger.error(s"Error while Returning Future in InsertOrders :: $exception ")
            InternalServerError(Json.obj("INSERT_ORDERS"->"ERROR"))
        }
      case JsError(errors) =>
        Logger.error(s"Error Validating Order $errors")
        Future.successful(BadRequest(Json.obj("INSERT_ORDERS"->"FAILURE")))

    }

  }



}

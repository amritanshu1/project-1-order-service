package controllers

import javax.inject.Inject
import org.joda.time.DateTime
import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc.{Action, AnyContent, Controller}
import weatherserviceFinal.WeatherService

import scala.concurrent.ExecutionContext.Implicits.global


class WeatherController @Inject()(weatherService: WeatherService) extends Controller {

  def fetchWeatherData(city: String, days: Int): Action[AnyContent] = Action.async { _ =>

    (for {
      cityForecastData <- weatherService.fetchWeatherData(city, days)
      forecastAdded <- weatherService.storeWeatherData(cityForecastData)
    } yield {}).map( _ =>
      Ok(Json.obj("FETCH_STORE_DATA"->"SUCCESS"))).recover {
      case exception =>
        Logger.error(s"${exception.getMessage}" )
        InternalServerError(Json.obj("FETCH_STORE_DATA" -> "ERROR"))
    }


     weatherService.fetchWeatherData(city, days).map (response => Ok(Json.toJson(response)))
  }
  def getWeatherDataHistory(from: String, to: String): Action[AnyContent] = Action.async{

    val weatherHistory = for {
        filteredForecastData <- weatherService.getWeatherDataHistory(DateTime.parse(from), DateTime.parse(to))
    } yield filteredForecastData

    weatherHistory.map(_ =>
      Ok(Json.obj("GET_HISTORY"->"SUCCESS"))).recover {
      case exception =>
        Logger.error(s"[getWeatherHistory]  ${exception.getMessage}")
        InternalServerError(Json.obj("GET_HISTORY" -> "ERROR"))
    }

  }

  def getWeatherDataForCity(city: String): Action[AnyContent] = Action.async { _ =>

    weatherService.getWeatherDataForCity(city).map ( _ =>
    Ok(Json.obj("GET_WEATHER_DATA_FOR_CITY"->"SUCCESS"))).recover {
      case exception =>
        Logger.error(s"[getWeatherforCity] $exception")
        InternalServerError(Json.obj("GET_WEATHER_DATA_FOR_CITY"->"ERROR"))
    }

  }

}

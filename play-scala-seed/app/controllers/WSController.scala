package controllers

import com.google.inject.Inject
import javax.inject.Singleton
import orderservice.OrderService
import orderservice.OrderService.Order
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.libs.ws.{WSClient, WSResponse}
import play.api.mvc.{Action, AnyContent, Controller}
import weatherservice_initial.WeatherService
import weatherservice_initial.WeatherService._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class WSController @Inject()(wsClient: WSClient, orderService: OrderService, weatherService: WeatherService) extends Controller {

  def getBody: Action[AnyContent] = Action.async {

    val r: Future[WSResponse] = wsClient.url("https://demo3588086.mockable.io/test").execute("GET")
    r.map(response => Ok(response.body))

  }

  def addDataFromAPI: Action[AnyContent] = Action.async {
    val futureResponse  = wsClient.url("https://demo3588086.mockable.io/test")
      .execute("GET").map{
      response => response.json.validate[Order]
    }

    futureResponse.flatMap {
      case JsSuccess(value, _) => orderService.addOrderDetails(value).map {
        case Left(str) => Ok(Json.toJson(str))
        case Right(_) => Ok(Json.toJson(s"Order Details Already Exist"))
      }
      case JsError(errors) => Future.successful(BadRequest(s"$errors"))
    }

  }


  def getWeather =  Action.async(parse.json) { request =>

    request.body.validate[Query] match {
      case JsSuccess(queryObj, _) =>
        weatherService.getWeather(queryObj.city, queryObj.days).map( response =>
          Ok(Json.toJson(response))
        )
      case JsError(errors) => Future.successful(BadRequest(s"$errors"))
    }
  }

  def getWeatherHistory(from: String, to:String) =  Action.async { _ =>

    weatherService.getWeatherHistory(from,to)
    Future.successful(Ok(s"from = $from , to = $to"))

  }



}

package controllers

import javax.inject._
import play.api._
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import play.api.mvc._
import services.{UserServicesPractice, service}
import service.Product.userWrites
import services.UserServicesPractice.{User, UserType}

import scala.collection.mutable
import scala.collection.mutable.Set
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */

@Singleton
class HomeController @Inject()(userService:UserServicesPractice) extends Controller {
  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index() = Action { implicit request: Request[AnyContent] =>
    Ok( views.html.index())
  }
  def about() = Action{ _ =>
    Ok( views.html.about() )
//    BadRequest
//    NoContent
//    Unauthorized
//    PaymentRequired
    Ok( getName() )
  }

  private def getName() = {
    " Returning INDIA "
  }

  def getProducts = Action.async {

    val serviceObj = new service()
    val prods = serviceObj.printProducts
    prods.map { x => Ok(Json.toJson(x)) }

  }

  def getPDF(uid: Long, userType: String) = Action.async {
    userService.getDocs(uid, userType).map {
      case Some(value) => Ok(Json.toJson(value))
      case None => BadRequest
    }
  }

  def getUsers: Action[AnyContent] = Action.async {
    userService.getAllUsers.map {
      case value => Ok(Json.toJson(value))
      case _ => BadRequest
    }
  }

  def createUser(): Action[JsValue] = Action.async(parse.json) { request =>
    val responseFut = request.body.validate[User] match {
      case JsSuccess(value, _) =>
        userService.createUser(value)
      case JsError(errors) =>
        Logger.error(s"Unable to create USER :: $errors")
        val x: Set[User] = Set()
        Future.successful(x)
    }

    responseFut.map{ x =>
      if(x.nonEmpty) Ok(Json.toJson(x))
      else BadRequest
    }

  }

  def deleteUser(): Action[JsValue] = Action.async(parse.json) { request =>
    val responseFut = request.body.validate[User] match {
      case JsSuccess(value, _) =>
        userService.deleteUser(value)
      case JsError(errors) =>
        Logger.error(s"Unable to Delete :: $errors")
        val x: mutable.Set[User] = mutable.Set()
        Future.successful(x)
    }

    responseFut.map{ x =>
      if(x.nonEmpty) Ok(Json.toJson(x))
      else BadRequest
    }
  }

  def createMultipleUser(): Action[JsValue] = Action.async(parse.json) { request =>
    val responseFut = request.body.validate[mutable.Set[User]] match {
      case JsSuccess(value, _) =>
        userService.createMultipleUser(value)
      case JsError(errors) =>
        Logger.error(s"Unable to Create Multiple Users $errors")
        val x: mutable.Set[User] = mutable.Set()
        Future.successful(x)
    }

    responseFut.map { x =>
      if(x.nonEmpty) Ok(Json.toJson(x))
      else BadRequest
    }

  }

  def deleteMultipleUser(): Action[JsValue] = Action.async(parse.json) { request =>
    val responseFut = request.body.validate[mutable.Set[User]] match {
      case JsSuccess(value, _) =>
        userService.deleteMultipleUser(value)
      case JsError(errors) =>
        Logger.error(s"Unable to Delete Multiple Users $errors")
        val x: mutable.Set[User] = mutable.Set()
        Future.successful(x)
    }

    responseFut.map{ x =>
      if(x.nonEmpty) Ok(Json.toJson(x))
      else BadRequest
    }
  }




}

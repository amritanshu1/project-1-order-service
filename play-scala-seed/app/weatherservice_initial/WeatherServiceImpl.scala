package weatherservice_initial

import com.google.inject.Inject
import javax.inject.Singleton
import play.api.Logger
import play.api.libs.json.JsValue
import play.api.libs.ws.{WSClient, WSResponse}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import WeatherService._

import scala.collection.mutable


@Singleton
class WeatherServiceImpl @Inject()(wsClient: WSClient) extends WeatherService {

  override def getAllWeatherData: Future[mutable.Set[CityData]] = {
    Future{cityDataSet}
  }

  override def getWeather(city: String, days: String): Future[mutable.Set[CityData]] = {


    val customURL = s"https://api.apixu.com/v1/forecast.json?key=db497699500a44239dc145208191208&q=$city&days=$days"
    val responseFut = wsClient.url(customURL).execute("GET")


    val cityDataFut = for {
      response <- responseFut.map(_.json)
    } yield {
      val city = (response \ "location" \ "name").as[String]
      val dateList = for {
        elem <- response \ "forecast" \ "forecastday" \\ "date"
      } yield {
        elem.as[String]
      }

      val TempList = for {
        elem <- response \ "forecast" \ "forecastday" \\ "day"
      } yield {
        ((elem \ "maxtemp_c").as[Double] , (elem \ "mintemp_c").as[Double])
      }

      val maxTempList = TempList.map(_._1)
      val minTempList = TempList.map(_._2)

      CityData(city, dateList.toList, maxTempList.toList, minTempList.toList)

    }

    getAllWeatherData.flatMap(x => cityDataFut.map(y => x += y))

  }

  override def getWeatherHistory(from: String, to: String) = {

    val weatherData = getAllWeatherData

    val newSet = for {
      dates    <- weatherData.map( obj => obj.map(_.dateList))
      maxTemps <- weatherData.map( obj => obj.map(_.maxTempList))
      minTemps <- weatherData.map( obj => obj.map(_.minTempList))
    } yield {
//      if (dates.contains(from))
        println( (dates,maxTemps,minTemps) )
    }

  }
}

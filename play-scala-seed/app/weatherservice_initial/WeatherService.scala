package weatherservice_initial

import com.google.inject.ImplementedBy
import play.api.libs.json.{JsPath, JsValue, Json, OFormat, Reads}
import weatherservice_initial.WeatherService.CityData
import scala.collection.mutable.Set
import scala.concurrent.Future


@ImplementedBy(classOf[WeatherServiceImpl])
trait WeatherService  {

  def getAllWeatherData: Future[Set[CityData]]
  def getWeather(city: String, days: String): Future[Set[CityData]]
  def getWeatherHistory(city: String, days: String)



}

object WeatherService {
  case class Query( city: String, days: String)

  object Query {
    implicit val queryFormat: OFormat[Query] = Json.format[Query]
  }

  case class CityData(name: String,
                      dateList: List[String],
                      maxTempList: List[Double],
                      minTempList: List[Double])

  object CityData{
    implicit val cityDataFormat: OFormat[CityData] = Json.format[CityData]
  }
  val cityDataSet: Set[CityData] = Set(
    CityData("Paris", List("2019-08-13","2019-08-14","2019-08-15"),
      List(28.9,31.2,32.1), List(24.9,19.2,22.1) ),
    CityData("Bangalore", List("2019-08-13","2019-08-14","2019-08-15","2019-08-16"),
      List(28.1,25.1,26.6,27.3), List(21,20.6,20.9,20.6) )
  )


}

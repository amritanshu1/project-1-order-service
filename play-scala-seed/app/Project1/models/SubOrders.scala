package Project1.models


import slick.jdbc.PostgresProfile.api._

case class SubOrderId ( id: Long )
object SubOrderId {
  implicit val subOrderIdColumnType: BaseColumnType[SubOrderId] =
    MappedColumnType.base[SubOrderId, Long](subOrderId => subOrderId.id,
      subOrderIdLong => SubOrderId(subOrderIdLong))
}

case class SellerId ( id: Long )
object SellerId {
  implicit val sellerIdColumnType: BaseColumnType[SellerId] =
    MappedColumnType.base[SellerId, Long]( sellerId => sellerId.id,
      sellerIdLong => SellerId(sellerIdLong))
}

sealed trait SubOrderStatus{
  def subOrderStatus : String
}

object SubOrderStatus {

  case object Accepted extends SubOrderStatus {
  override val subOrderStatus: String = "Accepted"
  }
  case object Rejected extends SubOrderStatus {
  override val subOrderStatus: String = "Rejected"
  }

  val subOrderStatusSet: Set[SubOrderStatus] = Set[SubOrderStatus](Accepted, Rejected)

  def toSuborderStatus( subOrderStatusStr: String): SubOrderStatus = subOrderStatusSet.collectFirst{
    case subOrderStatus if subOrderStatus.subOrderStatus == subOrderStatusStr => subOrderStatus
  }.getOrElse{
    throw new Exception("Incorrect SubOrderStatus Type String Provided")
  }

  implicit val subOrderStatusColumnType: BaseColumnType[SubOrderStatus] =
    MappedColumnType.base[SubOrderStatus, String](subOrderStatus => subOrderStatus.subOrderStatus,
      toSuborderStatus)

}



case class SubOrders( subOrderId: SubOrderId,
                      sellerId: SellerId,
                      subOrderStatus: SubOrderStatus)

class SubOrderTable(tag: Tag) extends Table[SubOrders] ( tag, "SubOrders" ) {

  def subOrderId = column[SubOrderId]("subOrderId")
  def sellerId = column[SellerId]("sellerId")
  def subOrderStatus = column[SubOrderStatus]("subOrderStatus")

  override def * = (subOrderId, sellerId, subOrderStatus)
    .<>(SubOrders.tupled, SubOrders.unapply)
}


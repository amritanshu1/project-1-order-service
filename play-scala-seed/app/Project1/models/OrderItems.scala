package Project1.models

import slick.jdbc.PostgresProfile.api._

case class OrderItemId ( orderItemId: Long )
object OrderItemId {
  implicit val orderItemIdColumnType: BaseColumnType[OrderItemId] =
    MappedColumnType.base[OrderItemId, Long](orderItemId => orderItemId.orderItemId,
      orderItemIdLong => OrderItemId(orderItemIdLong))
}

case class ProductId ( productId: Long )
object ProductId {
  implicit val productIdColumnType: BaseColumnType[ProductId] =
    MappedColumnType.base[ProductId, Long]( productId => productId.productId,
      productIdLong => ProductId(productIdLong))
}

case class OrderItems ( orderItemId: OrderItemId,
                        productId: ProductId,
                        productName: String,
                        quantity: Int,
                        price: BigDecimal)

class OrderItemTable (tag: Tag) extends Table[OrderItems] (tag, "orderItems") {

  def orderItemId = column[OrderItemId]("orderItemId")
  def productId = column[ProductId]("productId")
  def productName = column[String]("productName")
  def quantity = column[Int]("quantity")
  def price = column[BigDecimal]("price")

  override def * = (orderItemId, productId, productName, quantity, price)
    .<>(OrderItems.tupled, OrderItems.unapply)
}


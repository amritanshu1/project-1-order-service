package Project1.models

import java.sql.Timestamp

import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import play.api.libs.json.{JsResult, JsString, JsValue, Json, OFormat, Reads, Writes}
import slick.jdbc.PostgresProfile.api._

case class OrderId ( id : Long )
object OrderId {

  implicit val orderIdReads: Reads[OrderId] = new Reads[OrderId] {
    override def reads(json: JsValue): JsResult[OrderId] =
      json.validate[Long].map(OrderId(_))
  }

  implicit val orderIdWrites: Writes[OrderId] = new Writes[OrderId] {
    override def writes(o: OrderId): JsValue = Json.toJson(o.id)
  }

  implicit val orderIdColumnType: BaseColumnType[OrderId] =
    MappedColumnType.base[OrderId, Long](orderId => orderId.id, id => OrderId(id))
}

case class UserId ( id : Long )
object UserId {

  implicit val userIdReads: Reads[UserId] = new Reads[UserId] {
    override def reads(json: JsValue): JsResult[UserId] =
      json.validate[Long].map(UserId(_))
  }

  implicit val userIdWrites: Writes[UserId] = new Writes[UserId] {
    override def writes(o: UserId): JsValue = Json.toJson(o.id)
  }

  implicit val userIdColumnType: BaseColumnType[UserId] =
    MappedColumnType.base[UserId, Long](userId => userId.id, id => UserId(id))
}

case class UserAddressId ( addressId : Long )
object UserAddressId {

  implicit val userAddressIdReads: Reads[UserAddressId] = new Reads[UserAddressId] {
    override def reads(json: JsValue): JsResult[UserAddressId] =
      json.validate[Long].map(UserAddressId(_))
  }

  implicit val userAddressIdWrites: Writes[UserAddressId] = new Writes[UserAddressId] {
    override def writes(o: UserAddressId): JsValue = Json.toJson(o.addressId)
  }

  implicit val userAddressIdColumnType: BaseColumnType[UserAddressId] =
    MappedColumnType.base[UserAddressId, Long](userAddressId => userAddressId.addressId,
      addressId => UserAddressId(addressId))
}

sealed trait OrderStatus {
  def status : String
}

object OrderStatus {

  case object SUCCESS extends OrderStatus {
    override val status: String = "SUCCESS"
  }
  case object FAILED extends OrderStatus {
    override val status: String = "FAILED"
  }

  val orderStatusSet: Set[OrderStatus] = Set[OrderStatus](SUCCESS, FAILED)


  def toOrderStatus(orderStatusString: String): OrderStatus = orderStatusSet.collectFirst{
    case status if status.status == orderStatusString => status
  }.getOrElse{
    throw new Exception("Order Status not found for the given String")
  }

  implicit val orderStatusReads: Reads[OrderStatus] = new Reads[OrderStatus] {
    override def reads(json: JsValue): JsResult[OrderStatus] = json.validate[String].map(toOrderStatus)
  }

  implicit val orderStatusWrites: Writes[OrderStatus] = new Writes[OrderStatus] {
    override def writes(o: OrderStatus): JsValue = JsString(o.status)
  }

  implicit val orderStatusColumnType: BaseColumnType[OrderStatus] =
    MappedColumnType.base[OrderStatus, String](orderStatus => orderStatus.status, toOrderStatus)

}

sealed trait PaymentInstrument {
  def paymentType : String
}

object PaymentInstrument {

  case object CreditCard extends PaymentInstrument {
    override val paymentType: String = "CREDIT_CARD"
  }
  case object NetBanking extends PaymentInstrument {
    override val paymentType: String = "NET_BANKING"
  }

  val paymentTypeSet: Set[PaymentInstrument] = Set(CreditCard, NetBanking)

  def toPaymentInstrumentType(paymentInstrumentString: String): PaymentInstrument = paymentTypeSet.collectFirst{
    case paymentInstrumentType if paymentInstrumentType.paymentType == paymentInstrumentString => paymentInstrumentType
  }.getOrElse{
    throw new Exception("Incorrect Payment Instrument Type String Provided")
  }

  implicit val paymentInstrumentReads: Reads[PaymentInstrument] = new Reads[PaymentInstrument] {
    override def reads(json: JsValue): JsResult[PaymentInstrument] =
      json.validate[String].map(toPaymentInstrumentType)
  }

  implicit val paymentInstrumentWrites: Writes[PaymentInstrument] = new Writes[PaymentInstrument] {
    override def writes(o: PaymentInstrument): JsValue = JsString(o.paymentType)
  }



  implicit val paymentInstrumentColumnType: BaseColumnType[PaymentInstrument] =
    MappedColumnType.base[PaymentInstrument,String](
      PaymentInstrument => PaymentInstrument.paymentType,toPaymentInstrumentType )

}



case class Orders ( id: OrderId,
                    userId: UserId,
                    userAddressId: Option[UserAddressId],
                    status: OrderStatus,
                    paymentInstrument: PaymentInstrument,
                    orderedAt: DateTime)

object Orders {
//  implicit val dateTimeReads: Reads[DateTime] = play.api.libs.json.Reads.jodaDateReads("yyyy-MM-dd")

  val dateTimeFormatter: DateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd")

  implicit val dateTimeReads: Reads[DateTime] = new Reads[DateTime] {
    override def reads(json: JsValue): JsResult[DateTime] = json.validate[String].map{
      json => dateTimeFormatter.parseDateTime(json)
    }
  }

  implicit val dateTimeWrites: Writes[DateTime] = new Writes[DateTime] {
    override def writes(o: DateTime): JsValue = JsString(dateTimeFormatter.print(o))
  }

  implicit val dateTimeColumnType: BaseColumnType[DateTime] =
    MappedColumnType.base[DateTime,String](dt => dt.toString,
      dtStr => DateTime.parse(dtStr, dateTimeFormatter))


  implicit val ordersFormat: OFormat[Orders] = Json.format[Orders]



}


class OrdersTable(tag: Tag) extends Table[Orders] (tag,"Orders") {

  implicit val dateColumnType: BaseColumnType[DateTime] =
    MappedColumnType.base[ DateTime, Timestamp ]( dateTime => new Timestamp( dateTime.getMillis ),
      timestamp => new DateTime( timestamp.getTime ) )


  def id = column[OrderId]("orderId")
  def userId = column[UserId]("userId")
  def userAddressId = column[Option[UserAddressId]]("userAddressId")
  def status = column[OrderStatus]("orderStatus")
  def paymentInstrument = column[PaymentInstrument]("paymentInstrument")
  def orderedAt = column[DateTime]("orderedAt")


  override def * = (id, userId, userAddressId, status, paymentInstrument, orderedAt)
    .<>((Orders.apply _).tupled, Orders.unapply)
}
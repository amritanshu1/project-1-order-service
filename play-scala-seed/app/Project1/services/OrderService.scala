package Project1.services

import Project1.models._
import Project1.models.Orders
import com.google.inject.ImplementedBy
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Future

@ImplementedBy(classOf[OrderServiceImpl])
trait OrderService {

  def dbRun[T](action: DBIO[T]): Future[T]

  def insertOrderDBIO(ordersSeq: Seq[Orders]): DBIO[Unit]

  def insertOrder(orderSeq: Seq[Orders]) : Future[Unit]


}

object OrderService {

  lazy val orderTableQuery = TableQuery[OrdersTable]
  lazy val subOrderTableQuery = TableQuery[SubOrderTable]
  lazy val orderItemTableQuery = TableQuery[OrderItemTable]

  val createOrderTable = orderTableQuery.schema.create

//  val db = Database.forConfig("slick")
  val db = Database.forConfig("postgresDb")

}
package Project1.services

import Project1.models.Orders
import Project1.services.OrderService._
import com.google.inject.Singleton
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class OrderServiceImpl extends OrderService {

  override def dbRun[T](action: DBIO[T]): Future[T] = {
    db.run(action)
  }

  override def insertOrderDBIO(ordersSeq: Seq[Orders]): DBIO[Unit] = {
    (for {
      _ <- orderTableQuery.schema.create
      _ <- orderItemTableQuery.schema.create
      _ <- subOrderTableQuery.schema.create
      _ <- orderTableQuery ++= ordersSeq
    } yield ()).transactionally
  }


  override def insertOrder(orderSeq: Seq[Orders]): Future[Unit] = {
    dbRun(insertOrderDBIO(orderSeq))
  }

}

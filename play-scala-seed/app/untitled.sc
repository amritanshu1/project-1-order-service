// Udit's Assignment

//import EmployeeStatus.{CONTRACT, FULL_TIME, PART_TIME, PROBATION, TERMINATED}
import play.api.libs.json.{JsError, JsPath, JsResult, JsString, JsSuccess, JsValue, Json, OFormat, Reads, Writes}
//import play.db.Database
import slick.dbio.DBIO

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

import slick.driver.H2Driver.api._
import slick.jdbc.H2Profile


class EmployeeId (  id : String)

object EmployeeId {

  private def apply(id: String): EmployeeId = new EmployeeId(id)

  def apply(teamNameContainingOnlyAlphabets: String,
            employeeFirstName: String,
            employeeNumberInCompany: Int) = new
    EmployeeId(teamNameContainingOnlyAlphabets + "_" + employeeFirstName + "_" + employeeNumberInCompany.toString)

}

//val obj1 = EmployeeId("Back_End_20")
//obj1.id
val obj2 = EmployeeId("Backend" ,"John" ,37)
//obj2.id

/*

case class EmployeeId(id: String)

object EmployeeId {

  implicit val employeeIdRead: Reads[EmployeeId] = new Reads[EmployeeId] {
    override def reads(json: JsValue): JsResult[EmployeeId] = json.validate[String].map(EmployeeId(_))
  }

  def apply(teamNameContainingOnlyAlphabets: String,
            employeeFirstName: String,
            employeeNumberInCompany: Int) = new
    EmployeeId(teamNameContainingOnlyAlphabets + "_" + employeeFirstName + "_" + employeeNumberInCompany.toString)
}

sealed trait EmployeeStatus {
  def status: String
}

object EmployeeStatus {

  case object PROBATION extends EmployeeStatus {
    override def status = "PROBATION"
  }

  case object FULL_TIME extends EmployeeStatus {
    override def status = "FULL_TIME"
  }

  case object PART_TIME extends EmployeeStatus {
    override def status = "PART_TIME"
  }

  case object CONTRACT extends EmployeeStatus {
    override def status = "CONTRACT"
  }

  case object TERMINATED extends EmployeeStatus {
    override def status = "TERMINATED"
  }

  val statusSet: Set[EmployeeStatus] = Set(TERMINATED,CONTRACT, PART_TIME, FULL_TIME, PROBATION)

  implicit val clientReads: Reads[EmployeeStatus] = JsPath.read[String].map (toStatusType)
  implicit val clientWrites: Writes[EmployeeStatus] = new Writes[EmployeeStatus] {
    override def writes(o: EmployeeStatus): JsValue = JsString(o.status)
  }

  def toStatusType(str: String):EmployeeStatus = statusSet.collectFirst{
    case statusType if statusType.status.equals(str) => statusType
  }.getOrElse{
    throw new Exception("  ERROR in CONVERTING STATUS FROM STRING  ")
  }


}

case class Employee(id: EmployeeId, status: EmployeeStatus)

object Employee {

  implicit val employeeFormat: OFormat[Employee] = Json.format[Employee]

}

val employeeString =
    """
    \{
    "id":"normal_id_20",
    "status": "PROBATION"
    \}
    """




val json: String = """
  {
    "id" : "Back_End_20",
    "status" : "PROBATION"
  }
  """

val obj1 = Json.parse(json).validate[Employee] match {
  case JsSuccess(value, _) => value
  case JsError(errors) => throw new Exception(s"$errors")
}

obj1.id

println(1)



*/





/*


Future{ Future.sequence( List(1,2,3).map{ id =>
  Future{ s"Getting value for this id: $id" }} ) }.flatMap { f =>
  Future.failed[Int]( throw new Exception("Something Failed") ).flatMap{ f1 =>
    val v = Option("Some Random Variable").map(_ * f1)
    f.map( fv =>
      fv.flatMap( _.toList.map(_ * 2) ) ++ v.map(_.length))
  } }
*/




/*
import com.google.inject.{ImplementedBy, Inject}

trait Car {
  def fuelCheck: Double
}

trait BackCamera

//@ImplementedBy(classOf[HondaCity])
trait SedanCarParkingMixin {
  def parkSedan() : Unit
  def backCamera : BackCamera
}

class HondaCity @Inject()(  override val backCamera:BackCamera)  extends Car with SedanCarParkingMixin {

  override def fuelCheck: Double = ???

  override def parkSedan(): Unit = ???

}




*/





//import org.joda.time.DateTime
//import org.joda.time.format.DateTimeFormat
//
//val from = "2018-09-12"
//val d = DateTime.parse(from).toLocalDate
//d

//import scala.concurrent.ExecutionContext.Implicits.global
//import scala.concurrent.{Await, Future}
//import scala.concurrent.duration._
//
//
//import org.joda.time.DateTime
//import play.api.libs.json.{Format, JsString, JsValue, Json}
//
//import scala.concurrent.Future
//import scala.collection.mutable.Set
//
//
//sealed trait OrderStatus{
//  def asString: String
//}
//
//object OrderStatus {
//
//  private val allOrderStatuses = Set[OrderStatus](Started, Pending)
//
//  // here we could write in reads and casematch on strValue like write this in reads:
//  // json.validate[String].map{ strValue => strValue match {case "STARTED" =>  } }
//  // but suppose while matching
//  implicit val format: Format[OrderStatus] = new Format[OrderStatus] {
//
//    override def reads(json: JsValue) = {
//      json.validate[String].map { strValue =>
//        allOrderStatuses.collectFirst( new PartialFunction[OrderStatus,OrderStatus] {
//          override def isDefinedAt(x: OrderStatus):Boolean =
//            x.asString.equals(strValue)
//
//          override def apply(v1: OrderStatus):OrderStatus = v1
//        }).getOrElse(throw new Exception("OOPS"))
//      }
//    }
//
//    override def writes(o: OrderStatus) = {
//      JsString(o.asString)
//    }
//  }
//}
//
//case object Started extends OrderStatus {
//  override def asString = "STARTED"
//}
//case object Pending extends OrderStatus {
//  override def asString = "PENDING"
//}
//
//case class Name1 (firstName: String, lastName: String) {
//
//  assert(!firstName.contains("_")) // case class creation is failed if firstName contains underscore
//
//}
//case object Name1{
//  implicit val name1Format: Format[Name1] = new Format[Name1] {
//    override def writes(o: Name1) = {
//      JsString(o.firstName+"_"+o.lastName )
//    }
//
//    override def reads(json: JsValue) = {
//      json.validate[JsValue].map(  str => {
//        val s = str.as[String].split("_")
//        Name1(s(0), s(1) ) }
//      )
//    }
//    // ???? how to read
//  }
//}
//Json.toJson(Name1("n","tiwari"))
//
//
//case class Name(value: String)
//case object Name {
//  implicit val formats: Format[Name] = new Format[Name] {
//    override def reads(json: JsValue) =
//      json.validate[String].map { strValue =>
//        Name(strValue)
//      }
//
//    override def writes(o: Name): JsValue = {
//      JsString(o.value)
//    }
//  }
//  /*
//  implicit val nameFormat = Json.format[Name]
//  */
//}
//
//Json.toJson(Name("naman"))
//
//
//// to validate in request.body we have to have reads and writes
//
//case class EmployeeDetails(name: List[Name])
//object EmployeeDetails {
//  implicit val formats = Json.format[EmployeeDetails]
//}
//
//Json.toJson(EmployeeDetails(List(Name("Naman"), Name("Navjot"))))
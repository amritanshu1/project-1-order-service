package webservicesservice

import com.google.inject.ImplementedBy
import com.sun.xml.internal.ws.model.wsdl.WSDLServiceImpl
import play.api.libs.ws.WSResponse

import scala.concurrent.{ExecutionContext, Future}


@ImplementedBy(classOf[WSDLServiceImpl])
trait WSService {

  def callExternalServiceGet(url: String)/*(implicit ec: ExecutionContext)*/: Future[String]

  def getBodyFromExternalService(url:String):Future[WSResponse]


}

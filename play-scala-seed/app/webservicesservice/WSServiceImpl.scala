package webservicesservice
import com.google.inject.Inject
import javax.inject.Singleton
import play.api.libs.ws.{WSClient, WSResponse}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global


@Singleton
class WSServiceImpl @Inject()(wsClient: WSClient) extends WSService {

  override def callExternalServiceGet(url: String): Future[String] = {

    wsClient.url(url).execute("GET").map(wsResponse => wsResponse.body)

  }

  override def getBodyFromExternalService(url: String): Future[WSResponse] = {

    wsClient.url(url).execute("GET")

  }
}

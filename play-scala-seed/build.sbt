name := """play-scala-seed"""
organization := "zilingo.com"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.12"

//libraryDependencies += guice
//libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.3" % Test

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "zilingo.com.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "zilingo.com.binders._"

// https://mvnrepository.com/artifact/com.typesafe.play/play-ws
libraryDependencies += "com.typesafe.play" %% "play-ws" % "2.5.18"
//libraryDependencies += jdbc
// https://mvnrepository.com/artifact/com.typesafe.slick/slick
libraryDependencies += "com.typesafe.slick" %% "slick-hikaricp" % "3.2.3"
libraryDependencies += "com.h2database" % "h2" % "1.4.185"

libraryDependencies += "com.typesafe.play" %% "play-slick" % "2.0.0"
libraryDependencies += "com.typesafe.play" %% "play-slick-evolutions" % "2.0.0"

libraryDependencies += "org.postgresql" % "postgresql" % "42.2.1"
